﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ödev.Startup))]
namespace ödev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
